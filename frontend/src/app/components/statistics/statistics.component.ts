import { Component, Input, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { StatisticsService } from '../../services/statistics.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnDestroy {
  localStatistics$!: Observable<any>;

  private localStatistics = new BehaviorSubject({
    'friends': {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}},
    'auto-suggest': {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}},
    'friends-recommendation': {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}},
  });
  private statisticsSubscription!: Subscription;

  @Input({required: true}) useCase!: string;

  constructor(private statistics: StatisticsService) {
    this.statisticsSubscription = this.statistics.getStatistics().subscribe((s) => {
      this.localStatistics.next(s[this.useCase]);
    });

    this.localStatistics$ = this.localStatistics.asObservable();
  }

  ngOnDestroy(): void {
    this.statisticsSubscription.unsubscribe();
  }

  resetStatistics() {
    this.statistics.reset(this.useCase);
  }
}
