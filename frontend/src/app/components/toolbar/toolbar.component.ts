import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { DatabaseService } from '../../services/database.service';
import { Observable, Subscription } from 'rxjs';
import { HttpService } from '../../services/http.service';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  db: string = 'mysql';
  url: string = '/friends';
  user: number = 112;

  loading: Observable<boolean>;

  private routingSubscription: Subscription;

  constructor(
    public dialog: MatDialog,
    private databaseService: DatabaseService,
    private httpService: HttpService,
    private router: Router,
  ) {
    this.loading = this.httpService.isLoading();
    this.routingSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.url = event.urlAfterRedirects;
      }
    })
  }

  ngOnInit(): void {
    this.databaseService.getDatabase().subscribe((db) => {
      this.db = db;
    })
    this.databaseService.getUser().subscribe((userId) => {
      this.user = userId;
    })
  }

  ngOnDestroy(): void {
    this.routingSubscription.unsubscribe();
  }

  changeDatabase($event: MatButtonToggleChange) {
    this.databaseService.setDatabase($event.value);
  }

  changeUser($event: MatButtonToggleChange) {
    this.databaseService.setUser($event.value);
  }

  navigate($event: MatButtonToggleChange) {
    this.router.navigate([$event.value]);
  }

  openInfo() {
    let dialogRef = this.dialog.open(InfoDialogComponent, {
      height: '370px',
      width: '600px',
    });
  }
}
