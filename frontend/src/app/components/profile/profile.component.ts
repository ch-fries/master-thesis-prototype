import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { HttpService } from '../../services/http.service';
import { Observable, tap } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profile$!: Observable<any>;

  constructor(
    public dialogRef: MatDialogRef<ProfileComponent>,
    private httpService: HttpService,
    @Inject(MAT_DIALOG_DATA) public data: { id: number, person: any }
  ) {
  }

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.profile$ = this.httpService.getProfile(this.data.id);
  }

  openPerson(person: any) {
    this.dialogRef.close({ type: 'Person', id: person.id, person: person });
  }

  openTopic(topic: any) {
    this.dialogRef.close({ type: 'Topic', id: topic.id, topic: topic });
  }

  addAsFriend(id: number) {
    this.httpService.addAsFriend(id).subscribe((res) => {
      this.init();
    });
  }

  removeAsFriend(id: number) {
    this.httpService.removeAsFriend(id).subscribe((res) => {
      this.init();
    });
  }
}
