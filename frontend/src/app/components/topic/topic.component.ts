import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent {
  topic$!: Observable<any>;

  constructor(
    public dialogRef: MatDialogRef<TopicComponent>,
    private httpService: HttpService,
    @Inject(MAT_DIALOG_DATA) public data: { id: number, topic: any }
  ) {
  }

  ngOnInit(): void {
    this.init();
  }

  init(): void {
    this.topic$ = this.httpService.getTopic(this.data.id);
  }

  openPerson(person: any) {
    this.dialogRef.close({ type: 'Person', id: person.id, person: person });
  }

  like(id: number) {
    this.httpService.like(id).subscribe((res) => {
      this.init();
    });
  }
  unlike(id: number) {
    this.httpService.unlike(id).subscribe((res) => {
      this.init();
    });
  }
}
