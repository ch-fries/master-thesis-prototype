import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  private statistics: { [key: string]: any } = {
    'friends': {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}},
    'auto-suggest': {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}},
    'friends-recommendation': {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}},
    'friends-recommendation-2': {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}},
  }
  private statistics$ = new BehaviorSubject(this.statistics);

  constructor() {
  }

  public getStatistics(): Observable<any> {
    return this.statistics$.asObservable();
  }

  public save(useCase: string, db: string, duration: number, timestamp: number) {
    this.statistics[useCase][db].records.push(duration);
    if (this.statistics[useCase][db].records.length) {
      this.statistics[useCase][db].avg = Math.round(this.statistics[useCase][db].records.reduce((a: number, b: number) => (a + b)) / this.statistics[useCase][db].records.length);
    }
    this.statistics$.next(this.statistics);
  }

  reset(useCase: string) {
    this.statistics[useCase] = {mysql: {records: [], avg: 0}, neo4j: {records: [], avg: 0}};
    this.statistics$.next(this.statistics);
  }
}
