import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { StatisticsService } from './statistics.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private statistics: StatisticsService) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map(event => this.logResponse(event)),
    );
  }

  private logResponse(event: HttpResponse<any> | any) {
    if (event instanceof HttpResponse) {
      if (event.url === null) {
        return event;
      }
      const url = event.url.split('?')[0];
      const urlParts = url.split('/');
      const useCase = urlParts[urlParts.length - 1];
      const db = urlParts[urlParts.length - 2];

      if (useCase === 'profile' || useCase === 'topic' ||  useCase === 'add-friend' ||  useCase === 'remove-friend' ||  useCase === 'like'||  useCase === 'unlike') {
        return event;
      }

      this.statistics.save(useCase, db, Math.round(event.body.duration), Date.now())
    }
    return event;
  }
}
