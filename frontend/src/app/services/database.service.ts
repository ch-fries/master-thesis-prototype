import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private databaseSubject = new BehaviorSubject<string>('mysql');
  private userSubject = new BehaviorSubject<number>(112);

  constructor() {
  }

  public getDatabase(): Observable<string> {
    return this.databaseSubject.asObservable();
  }

  public setDatabase(db: string) {
    this.databaseSubject.next(db);
  }

  public getUser(): Observable<number> {
    return this.userSubject.asObservable();
  }

  public setUser(userId: number) {
    this.userSubject.next(userId);
  }
}
