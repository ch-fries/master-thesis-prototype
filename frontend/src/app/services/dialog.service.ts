import { Injectable } from '@angular/core';
import { ProfileComponent } from '../components/profile/profile.component';
import { MatDialog } from '@angular/material/dialog';
import { TopicComponent } from '../components/topic/topic.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {
  constructor(private dialog: MatDialog) {
  }

  openProfile(id: number, friend: any) {
    let dialogRef = this.dialog.open(ProfileComponent, {
      autoFocus: false,
      data: {
        id: id,
        person: friend,
      },
      height: '780px',
      width: '980px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.hasOwnProperty('id')) {
        if (result.type === 'Person') {
          this.openProfile(result.id, result.person);
        }
        if (result.type === 'Topic') {
          this.openTopic(result.id, result.topic);
        }
      }
    })
  }

  openTopic(id: number, topic: any) {
    let dialogRef = this.dialog.open(TopicComponent, {
      autoFocus: false,
      data: {
        id: id,
        topic: topic,
      },
      height: '780px',
      width: '980px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.hasOwnProperty('id')) {
        if (result.type === 'Person') {
          this.openProfile(result.id, result.person);
        }
        if (result.type === 'Topic') {
          this.openTopic(result.id, result.topic);
        }
      }
    })
  }
}
