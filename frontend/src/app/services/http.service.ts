import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, Observable, tap, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private baseUrl: string = 'https://master-thesis.ddev.site/api'
  private db: string = 'mysql';
  private userId: number = 112;
  private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private databaseService: DatabaseService,
    private http: HttpClient,
  ) {
    this.databaseService.getDatabase().subscribe((db) => {
      this.db = db;
    })
    this.databaseService.getUser().subscribe((userId) => {
      this.userId = userId;
    })
  }

  public getFriends(): Observable<any> {
    this.setLoading(true);
    return this.http.get<any>(this.getFriendsUrl()).pipe(
      tap(() => this.setLoading(false)),
      catchError(err => {
        this.setLoading(false);
        throw err;
      })
    );
  }

  public getFriendsRecommendation(): Observable<any[]> {
    this.setLoading(true);
    return this.http.get<any[]>(this.getFriendsRecommendationUrl()).pipe(
      tap(result => this.setLoading(false)),
      catchError((err) => this.catchError(err))
    );
  }

  public getFriendsRecommendation2(): Observable<any[]> {
    this.setLoading(true);
    return this.http.get<any[]>(this.getFriendsRecommendation2Url()).pipe(
      tap(result => this.setLoading(false)),
      catchError((err) => this.catchError(err))
    );
  }

  public autoSuggest(searchQuery: string): Observable<any[]> {
    this.setLoading(true);
    return this.http.get<any[]>(this.getAutoSuggestUrl(searchQuery)).pipe(
      tap(result => this.setLoading(false)),
      catchError((err) => this.catchError(err))
    );
  }

  public getProfile(id: number): Observable<any[]> {
    return this.http.get<any[]>(this.getProfileUrl(id)).pipe(
      catchError((err) => this.catchError(err))
    );
  }

  public getTopic(id: number): Observable<any[]> {
    return this.http.get<any[]>(this.getTopicUrl(id)).pipe(
      catchError((err) => this.catchError(err))
    );
  }

  public addAsFriend(id: number): Observable<any[]> {
    return this.http.post<any[]>(this.getAddAsFriendUrl(id), {}).pipe(
      catchError((err) => this.catchError(err))
    );
  }

  public removeAsFriend(id: number): Observable<any[]> {
    return this.http.delete<any[]>(this.getRemoveAsFriendUrl(id), {}).pipe(
      catchError((err) => this.catchError(err))
    );
  }

  public like(id: number): Observable<any[]> {
    return this.http.post<any[]>(this.getLikeUrl(id), {}).pipe(
      catchError((err) => this.catchError(err))
    );
  }

  public unlike(id: number): Observable<any[]> {
    return this.http.delete<any[]>(this.getUnlikeUrl(id), {}).pipe(
      catchError((err) => this.catchError(err))
    );
  }

  public isLoading(): Observable<boolean> {
    return this.loading.asObservable();
  }

  private getFriendsUrl(): string {
    return `${this.baseUrl}/${this.db}/friends?userId=${this.userId}`;
  }

  private getFriendsRecommendationUrl(): string {
    return `${this.baseUrl}/${this.db}/friends-recommendation?userId=${this.userId}`;
  }

  private getFriendsRecommendation2Url(): string {
    return `${this.baseUrl}/${this.db}/friends-recommendation-2?userId=${this.userId}`;
  }

  private getAutoSuggestUrl(searchQuery: string): string {
    return `${this.baseUrl}/${this.db}/auto-suggest?userId=${this.userId}&s=` + searchQuery;
  }

  private getProfileUrl(id: number) {
    return `${this.baseUrl}/${this.db}/profile?userId=${this.userId}&profileId=` + id;
  }

  private getTopicUrl(id: number) {
    return `${this.baseUrl}/${this.db}/topic?userId=${this.userId}&topicId=` + id;
  }

  private getAddAsFriendUrl(id: number) {
    return `${this.baseUrl}/${this.db}/friend?userId=${this.userId}&profileId=` + id;
  }

  private getRemoveAsFriendUrl(id: number) {
    return `${this.baseUrl}/${this.db}/friend?userId=${this.userId}&profileId=` + id;
  }

  private getLikeUrl(id: number) {
    return `${this.baseUrl}/${this.db}/like?userId=${this.userId}&topicId=` + id;
  }

  private getUnlikeUrl(id: number) {
    return `${this.baseUrl}/${this.db}/like?userId=${this.userId}&topicId=` + id;
  }

  private setLoading(loading: boolean) {
    this.loading.next(loading);
  }

  private catchError(error: any) {
    this.setLoading(false);

    return throwError(error);
  }
}
