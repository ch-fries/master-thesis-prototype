import { Component } from '@angular/core';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private dialog: MatDialog) {
  }

  openInfo() {
    let dialogRef = this.dialog.open(InfoDialogComponent, {
      height: '370px',
      width: '600px',
    });
  }
}
