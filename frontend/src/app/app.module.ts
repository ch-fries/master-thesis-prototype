import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { MaterialModule } from './material/material.module';
import { FormsModule } from '@angular/forms';
import { FriendsListComponent } from './pages/friends-list/friends-list.component';
import { FriendsRecommendationComponent } from './pages/friends-recommendation/friends-recommendation.component';
import { AutosuggestComponent } from './pages/autosuggest/autosuggest.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { ProfileComponent } from './components/profile/profile.component';
import { TopicComponent } from './components/topic/topic.component';
import { FriendsRecommendationUnionComponent } from './pages/friends-recommendation-union/friends-recommendation-union.component';
import { HomeComponent } from './pages/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    FriendsListComponent,
    FriendsRecommendationComponent,
    AutosuggestComponent,
    StatisticsComponent,
    InfoDialogComponent,
    ProfileComponent,
    TopicComponent,
    FriendsRecommendationUnionComponent,
    HomeComponent
  ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
    ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
