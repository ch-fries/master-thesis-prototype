import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { DatabaseService } from '../../services/database.service';
import { HttpService } from '../../services/http.service';
import { DialogService } from '../../services/dialog.service';

@Component({
  selector: 'app-friends-recommendation-union',
  templateUrl: './friends-recommendation-union.component.html',
  styleUrls: ['./friends-recommendation-union.component.scss']
})
export class FriendsRecommendationUnionComponent implements OnInit, OnDestroy {
  public db: string = 'mysql';
  public friends$!: Observable<any>;

  private dbSubscription!: Subscription;
  private userSubscription!: Subscription;

  constructor(
    private databaseService: DatabaseService,
    private dialogService: DialogService,
    private httpService: HttpService) {
  }

  ngOnInit(): void {
    this.init();
    this.dbSubscription = this.databaseService.getDatabase().subscribe((db) => {
      this.db = db;
      this.init();
    })
    this.userSubscription = this.databaseService.getUser().subscribe((userId) => {
      this.init();
    })
  }

  ngOnDestroy(): void {
    this.dbSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

  private init() {
    this.friends$ = this.httpService.getFriendsRecommendation2();
  }

  runAgain() {
    this.init();
  }

  openProfile(id: number, friend: any) {
    this.dialogService.openProfile(id, friend);
  }
}
