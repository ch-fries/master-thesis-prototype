import { Component } from '@angular/core';
import { BehaviorSubject, debounceTime, distinctUntilChanged, Observable, Subscription } from 'rxjs';
import { DatabaseService } from '../../services/database.service';
import { HttpService } from '../../services/http.service';
import { DialogService } from '../../services/dialog.service';

@Component({
  selector: 'app-autosuggest',
  templateUrl: './autosuggest.component.html',
  styleUrls: ['./autosuggest.component.scss']
})
export class AutosuggestComponent {
  public db: string = 'mysql';
  public friends$!: Observable<any>;

  private dbSubscription!: Subscription;

  private results = new BehaviorSubject<any>([]);
  public results$!: Observable<any>;
  public searchQuery: string = '';
  public search = new BehaviorSubject<string>('');

  constructor(
    private databaseService: DatabaseService,
    private dialogService: DialogService,
    private httpService: HttpService) {
  }

  ngOnInit(): void {
    this.dbSubscription = this.databaseService.getDatabase().subscribe((db) => {
      this.db = db;
    })

    this.results$ = this.results.asObservable();

    this.search.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(value => {
      if (value.length < 2) {
        return;
      }
      this.httpService.autoSuggest(value).subscribe(result => {
        this.results.next(result);
      });
    });
  }

  ngOnDestroy(): void {
    this.dbSubscription.unsubscribe();
  }

  private init() {
    this.httpService.autoSuggest(this.searchQuery).subscribe(result => {
      this.results.next(result);
    });
  }

  runAgain() {
    this.init();
  }

  open(item: any) {
    if (item.type === 'Topic') {
      this.dialogService.openTopic(item.id, { id: item.id, name: item.label });
    } else {
      this.dialogService.openProfile(item.id, item);
    }
  }
}
