import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutosuggestComponent } from './pages/autosuggest/autosuggest.component';
import { FriendsRecommendationComponent } from './pages/friends-recommendation/friends-recommendation.component';
import { FriendsListComponent } from './pages/friends-list/friends-list.component';
import {
  FriendsRecommendationUnionComponent
} from './pages/friends-recommendation-union/friends-recommendation-union.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: 'friends', component: FriendsListComponent },
  { path: 'autosuggest', component: AutosuggestComponent },
  { path: 'friends-recommendation', component: FriendsRecommendationComponent },
  { path: 'friends-recommendation-2', component: FriendsRecommendationUnionComponent },
  { path: '**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
