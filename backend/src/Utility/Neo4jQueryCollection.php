<?php

namespace App\Utility;

class Neo4jQueryCollection
{
    public static function getFriendsListQuery(int $userId): string
    {
        return <<<NEO4J
            MATCH 
                (me:Person {personId: $userId})-[:IS_FRIENDS_WITH]-(p:Person) 
            RETURN p.personId, p.firstName, p.lastName
            ORDER BY p.personId;
        NEO4J;
    }

    public static function getFriendsRecommendationQuery(int $userId): string
    {
        // Alternative based on apoc (d=2 ca 27 ms, d=3 ca 25ms)
        return <<<NEO4J
            MATCH (me:Person {personId: $userId})
            CALL apoc.path.expandConfig(me, { 
                relationshipFilter: "IS_FRIENDS_WITH", 
                minLevel: 1, 
                maxLevel: 3,
                uniqueness: "NODE_GLOBAL"
            })
            YIELD path
            WITH me, apoc.path.elements(path) AS pathElements, path
            WITH me, length(path) AS distance, pathElements[size(pathElements) - 1] AS friend
            OPTIONAL MATCH mfs=(me)-[:IS_FRIENDS_WITH]-(mf)-[:IS_FRIENDS_WITH]-(friend:Person)
            WHERE distance = 2
            RETURN friend, distance, count(DISTINCT mfs) AS mutualFriends
            ORDER BY mutualFriends DESC, distance, friend.firstName, friend.lastName
            LIMIT 100
        NEO4J;
    }

    public static function getFriendsRecommendation2Query(int $userId): string
    {
        return <<<NEO4J
            MATCH (me:Person { personId: $userId })
            CALL {
                WITH me
                OPTIONAL MATCH pMutualFriends=(me)-[:IS_FRIENDS_WITH]-(mf:Person)-[:IS_FRIENDS_WITH]-(friend:Person)
                WHERE NOT EXISTS  ((me)-[:IS_FRIENDS_WITH]-(friend))
                RETURN friend, count(DISTINCT pMutualFriends) * 0.25 AS score
            UNION ALL
                WITH me
                OPTIONAL MATCH pSameCountry=(me)-[:LIVES_IN]->(c:Country)<-[:LIVES_IN]-(friend:Person)
                WHERE NOT EXISTS ((me)-[:IS_FRIENDS_WITH]-(friend))
                RETURN friend, count(DISTINCT pSameCountry) AS score
            UNION ALL
                WITH me
                OPTIONAL MATCH pTopics=(me)-[:LIKES]->(topic:Topic)<-[:LIKES]-(friend:Person)
                WHERE NOT EXISTS ((me)-[:IS_FRIENDS_WITH]-(friend))
                RETURN friend, count(DISTINCT pTopics) * 5 AS score
            }
            RETURN DISTINCT friend, sum(score) AS score
            ORDER BY score DESC
            LIMIT 100
        NEO4J;
    }

    public static function getAutoSuggestQuery(int $userId, string $searchQuery): string
    {
        return <<<NEO4J
            CALL {
                MATCH (f:Person)
                WITH toLower(f.firstName + ' ' + f.lastName) AS s, f
                WHERE s CONTAINS '$searchQuery'
                WITH s, f, exists((f)-[:IS_FRIENDS_WITH]-(:Person {personId: $userId})) AS connected, 'Person' AS type
                RETURN f.personId AS id, f.firstName + ' ' + f.lastName AS label, connected, type,
                    (CASE WHEN s = '$searchQuery' THEN 10
                    WHEN s STARTS WITH '$searchQuery' THEN 5
                    WHEN s ENDS WITH '$searchQuery' THEN 1
                    ELSE 3 END) AS score
                ORDER BY
                    connected DESC,
                    (CASE WHEN s = '$searchQuery' THEN 1
                    WHEN s STARTS WITH '$searchQuery' THEN 2
                    WHEN s ENDS WITH '$searchQuery' THEN 4
                    ELSE 3 END), f.firstName, f.lastName
                LIMIT 5
                UNION
                MATCH (t:Topic)
                WITH t, toLower(t.name) AS s 
                WHERE s CONTAINS '$searchQuery'
                WITH t, s, exists((t)<-[:LIKES]-(:Person {personId: $userId})) AS connected, 'Topic' AS type
                RETURN t.topicId AS id, t.name AS label, connected, type,
                    (CASE WHEN s = '$searchQuery' THEN 10
                    WHEN s STARTS WITH '$searchQuery' THEN 5
                    WHEN s ENDS WITH '$searchQuery' THEN 1
                    ELSE 3 END) AS score
                ORDER BY 
                    connected DESC,
                    (CASE WHEN s = '$searchQuery' THEN 1
                    WHEN s STARTS WITH '$searchQuery' THEN 2
                    WHEN s ENDS WITH '$searchQuery' THEN 4
                    ELSE 3 END), s
                LIMIT 5
            }
            RETURN id, label, connected, score, type
            ORDER BY connected DESC, score DESC
        NEO4J;
    }

    public static function getPerson(int $userId, int $profileId): string
    {
        return <<<NEO4J
            MATCH(p:Person {personId: $profileId})
            RETURN p, exists((p)-[:IS_FRIENDS_WITH]-(:Person {personId: $userId})) AS isFriend
        NEO4J;
    }

    public static function getTopic(int $userId, int $topicId): string
    {
        return <<<NEO4J
            MATCH (t:Topic {topicId: $topicId})
            RETURN t, exists((t)<-[:LIKES]-(:Person {personId: $userId})) AS likes
        NEO4J;
    }

    public static function getTopics(int $userId, int $profileId): string
    {
        return <<<NEO4J
            MATCH (t:Topic)
            WHERE (t)<-[:LIKES]-(:Person {personId: $profileId})
            RETURN t, exists((t)<-[:LIKES]-(:Person {personId: $userId})) AS mutual
            ORDER BY mutual DESC, t.name
        NEO4J;
    }

    public static function getFriends(int $userId, int $profileId): string
    {
        return <<<NEO4J
            MATCH (p:Person)
            WHERE (p)-[:IS_FRIENDS_WITH]-(:Person {personId: $profileId})
            RETURN p, exists((p)-[:IS_FRIENDS_WITH]-(:Person {personId: $userId})) AS mutual
            ORDER BY mutual DESC, p.firstName, p.lastName
        NEO4J;
    }

    public static function getPeopleLikingTopic(int $userId, int $topicId): string
    {
        return <<<NEO4J
            MATCH (p:Person)
            WHERE (p)-[:LIKES]->(:Topic {topicId: $topicId}) AND p.personId <> $userId
            RETURN p, exists((p)-[:IS_FRIENDS_WITH]-(:Person {personId: $userId})) AS mutual
            ORDER BY mutual DESC, p.firstName, p.lastName
            LIMIT 200
        NEO4J;
    }

    public static function addFriend(int $userId, int $profileId): string
    {
        $now = (new \DateTime('now'))->format('Y-m-d H:i:s');
        $now = str_replace(' ', 'T', $now);
        return <<<NEO4J
            MATCH
              (a:Person),
              (b:Person)
            WHERE a.personId = $userId AND b.personId = $profileId
            CREATE (a)-[r:IS_FRIENDS_WITH { createdAt: datetime('$now')}]->(b)
            RETURN type(r)
        NEO4J;
    }

    public static function removeFriend(int $userId, int $profileId): string
    {
        return <<<NEO4J
            MATCH (a:Person {personId: $userId})-[r:IS_FRIENDS_WITH]-(b:Person {personId: $profileId})
            DELETE r
        NEO4J;
    }

    public static function like(int $userId, int $profileId): string
    {
        $now = (new \DateTime('now'))->format('Y-m-d H:i:s');
        $now = str_replace(' ', 'T', $now);
        return <<<NEO4J
            MATCH
              (p:Person),
              (t:Topic)
            WHERE p.personId = $userId AND t.topicId = $profileId
            CREATE (p)-[r:LIKES { createdAt: datetime('$now')}]->(t)
            RETURN type(r)
        NEO4J;
    }

    public static function unlike(int $userId, int $profileId): string
    {
        return <<<NEO4J
            MATCH (p:Person {personId: $userId})-[r:LIKES]-(t:Topic {topicId: $profileId})
            DELETE r
        NEO4J;
    }
}
