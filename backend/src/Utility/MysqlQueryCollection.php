<?php

namespace App\Utility;

class MysqlQueryCollection
{
    public static function getFriendsListQuery(int $userId): string
    {
        return <<<SQL
            SELECT p.id, p.first_name, p.last_name 
            FROM person AS p 
                INNER JOIN friendship AS f ON p.id = f.target_id 
            WHERE f.origin_id = $userId
            ORDER BY p.id
        SQL;
    }

    public static function getFriendsRecommendationQuery(int $userId): string
    {
        return <<<SQL
            WITH RECURSIVE paths (current_target_id, distance, path) AS (
              SELECT origin_id, 0, CAST(origin_id AS CHAR(100))
              FROM friendship
              WHERE origin_id = $userId
              UNION DISTINCT
              SELECT friendship.target_id, distance + 1, CONCAT(paths.path, ',', friendship.target_id)
              FROM paths
              JOIN friendship ON current_target_id = friendship.origin_id
              WHERE distance < 3
              AND NOT FIND_IN_SET(friendship.target_id, path)
            )
            SELECT DISTINCT p.*, distance, path, mut.mutual_friends
            FROM paths
            JOIN person p ON p.id = current_target_id
            LEFT JOIN (
                SELECT mf1.origin_id AS personId, count(*) AS mutual_friends, 2 AS dist
                FROM friendship AS mf1
                JOIN friendship AS mf2
                ON mf1.target_id = mf2.origin_id AND mf1.origin_id <> mf2.target_id
                WHERE mf2.target_id = $userId
                AND mf1.origin_id NOT IN (SELECT f.target_id FROM friendship f WHERE f.origin_id = $userId)
                GROUP BY mf1.origin_id, mf2.target_id
            ) AS mut ON mut.personId = p.id AND mut.dist = distance
            WHERE current_target_id <> $userId AND p.id NOT IN (SELECT target_id FROM friendship WHERE origin_id = $userId)
            GROUP BY p.id
            ORDER BY mut.mutual_friends DESC, distance, p.first_name, p.last_name
            LIMIT 100;
        SQL;
    }

    public static function getFriendsRecommendation2Query(int $userId): string
    {
        return <<<SQL
            SELECT SUM(score) AS finalScore, p.* 
            FROM (
                SELECT DISTINCT p.*, COUNT(*) * 5 AS score
                FROM person AS p
                JOIN `like` AS l ON p.id = l.person_id
                JOIN topic AS t ON l.topic_id = t.id
                WHERE t.id IN (SELECT topic_id FROM `like` WHERE person_id = $userId)
                AND p.id <> $userId
                AND p.id NOT IN (SELECT target_id FROM friendship WHERE origin_id = $userId)
                GROUP BY p.id
                
                UNION ALL
                
                SELECT DISTINCT p.*, 1 AS score
                FROM person AS p
                WHERE p.country = (SELECT country FROM person WHERE id = $userId)
                AND p.id <> $userId
                AND p.id NOT IN (SELECT target_id FROM friendship WHERE origin_id = $userId)
                
                UNION ALL
                
                SELECT DISTINCT p.*, mut.mutual_friends * 0.25 AS score
                FROM person AS p
                JOIN friendship AS f1 ON p.id = f1.target_id
                JOIN (
                    SELECT mf1.origin_id, mf2.origin_id AS personId, count(*) AS mutual_friends
                    FROM friendship AS mf1
                    JOIN friendship AS mf2 ON mf1.target_id = mf2.target_id AND mf1.origin_id <> mf2.origin_id
                    WHERE mf1.origin_id = $userId
                    AND mf2.origin_id NOT IN (SELECT f.target_id AS friend_id_2 FROM friendship f WHERE f.origin_id = $userId)
                    GROUP BY mf1.origin_id, mf2.origin_id
                ) AS mut ON mut.personId = f1.target_id
                WHERE f1.origin_id IN (SELECT f.target_id AS friend_id FROM friendship f WHERE f.origin_id = $userId)
                AND f1.target_id NOT IN (SELECT f.target_id AS friend_id_2 FROM friendship f WHERE f.origin_id = $userId)
                AND f1.target_id <> $userId
            ) AS p
            GROUP BY p.id
            ORDER BY finalScore DESC, p.first_name, p.last_name
            LIMIT 100
        SQL;
    }

    public static function getAutoSuggestQuery(int $userId, string $searchQuery): string
    {
        return <<<SQL
            (SELECT 
                CONCAT(p.first_name, ' ', p.last_name) AS label,
                (CASE WHEN f.origin_id IS NOT NULL THEN true ELSE false END) AS connected,
                (CASE 
                    WHEN CONCAT(p.first_name, ' ', p.last_name) LIKE '$searchQuery' THEN 10
                    WHEN CONCAT(p.first_name, ' ', p.last_name) LIKE '$searchQuery%' THEN 5
                    WHEN CONCAT(p.first_name, ' ', p.last_name) LIKE '%$searchQuery' THEN 1
                    ELSE 3
                END) AS score, 'Person' AS type, p.id
            FROM person AS p 
            LEFT JOIN friendship AS f ON p.id = f.target_id AND f.origin_id = $userId
            WHERE 
                CONCAT(p.first_name, ' ', p.last_name) LIKE '%$searchQuery%'
            ORDER BY 
                (CASE WHEN origin_id IS NOT NULL THEN 1 ELSE 2 END),
                (CASE
                    WHEN CONCAT(p.first_name, ' ', p.last_name) LIKE '$searchQuery' THEN 1
                    WHEN CONCAT(p.first_name, ' ', p.last_name) LIKE '$searchQuery%' THEN 2
                    WHEN CONCAT(p.first_name, ' ', p.last_name) LIKE '%$searchQuery' THEN 4
                    ELSE 3
                END), first_name, last_name
            LIMIT 5)
            UNION
            (SELECT
                t.name AS label,
                (CASE WHEN l.person_id IS NOT NULL THEN true ELSE false END) AS connected,
                (CASE 
                    WHEN t.name LIKE '$searchQuery' THEN 10
                    WHEN t.name LIKE '$searchQuery%' THEN 5
                    WHEN t.name LIKE '%$searchQuery' THEN 1
                    ELSE 3
                END) AS score, 'Topic' AS type, t.id
            FROM topic AS t 
            LEFT JOIN `like` AS l ON t.id = l.topic_id AND l.person_id = $userId
            WHERE 
                t.name LIKE '%$searchQuery%'
            ORDER BY
                (CASE WHEN l.person_id IS NOT NULL THEN 1 ELSE 2 END),
                (CASE
                    WHEN t.name LIKE '$searchQuery' THEN 1
                    WHEN t.name LIKE '$searchQuery%' THEN 2
                    WHEN t.name LIKE '%$searchQuery' THEN 4
                    ELSE 3
                END), t.name LIMIT 5)
            ORDER BY connected DESC, score DESC
        SQL;
    }

    public static function getPerson(int $userId, int $profileId): string
    {
        return <<<SQL
            SELECT 
                p.id AS id, p.first_name AS firstName, p.last_name AS lastName, p.gender,
                (CASE WHEN f.target_id IS NOT NULL THEN true ELSE false END) AS isFriend
            FROM person AS p
            LEFT JOIN friendship AS f ON p.id = f.origin_id AND f.target_id = $userId
            WHERE p.id = $profileId
        SQL;
    }

    public static function getTopic(int $userId, int $topicId): string
    {
        return <<<SQL
            SELECT 
                t.*,
                (CASE WHEN l.person_id IS NOT NULL THEN true ELSE false END) AS likes
            FROM topic AS t
            LEFT JOIN `like` AS l ON l.topic_id = t.id AND l.person_id = $userId
            WHERE t.id = $topicId
        SQL;
    }

    public static function getTopics(int $userId, int $profileId): string
    {
        return <<<SQL
            SELECT 
                t.*,
                (CASE WHEN l2.person_id IS NOT NULL THEN true ELSE false END) AS mutual
            FROM topic AS t
            JOIN `like` AS l ON t.id = l.topic_id AND l.person_id = $profileId
            LEFT JOIN `like` AS l2 ON l.topic_id = l2.topic_id AND l2.person_id = $userId
            ORDER BY mutual DESC, t.name
        SQL;
    }

    public static function getFriends(int $userId, int $profileId): string
    {
        return <<<SQL
            SELECT 
                p.*,
                (CASE WHEN f2.target_id IS NOT NULL THEN true ELSE false END) AS mutual
            FROM person AS p     
            JOIN friendship AS f ON p.id = f.target_id AND f.origin_id = $profileId
            LEFT JOIN friendship AS f2 ON f.target_id = f2.origin_id AND f2.target_id = $userId
            ORDER BY mutual DESC, p.first_name, p.last_name
        SQL;
    }

    public static function getPeopleLikingTopic(int $userId, int $topicId): string
    {
        return <<<SQL
            SELECT 
                p.*,
                (CASE WHEN f.target_id IS NOT NULL THEN true ELSE false END) AS mutual
            FROM person AS p     
            JOIN `like` AS l ON p.id = l.person_id AND l.topic_id = $topicId
            LEFT JOIN friendship AS f ON f.origin_id = p.id AND f.target_id = $userId
            WHERE p.id <> $userId
            ORDER BY mutual DESC, p.first_name, p.last_name
            LIMIT 200
        SQL;
    }

    public static function addFriend(int $userId, int $profileId): string
    {
        $now = (new \DateTime('now'))->format('Y-m-d H:i:s');
        return <<<SQL
            INSERT INTO friendship (origin_id, target_id, created_at) VALUES ($userId, $profileId, '$now'), ($profileId, $userId, '$now')
        SQL;
    }

    public static function removeFriend(int $userId, int $profileId): string
    {
        return <<<SQL
            DELETE FROM friendship
            WHERE (origin_id = $userId AND target_id = $profileId) OR (target_id = $userId AND origin_id = $profileId)
        SQL;
    }

    public static function like(int $userId, int $profileId): string
    {
        $now = (new \DateTime('now'))->format('Y-m-d H:i:s');
        return <<<SQL
            INSERT INTO `like` (person_id, topic_id, created_at) VALUES ($userId, $profileId, '$now')
        SQL;
    }

    public static function unlike(int $userId, int $profileId): string
    {
        return <<<SQL
            DELETE FROM `like`
            WHERE person_id = $userId AND topic_id = $profileId
        SQL;
    }
}
