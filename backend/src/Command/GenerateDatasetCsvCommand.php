<?php

namespace App\Command;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:generate-dataset-csv',
    description: 'Generate a dataset and store it as CSV files',
)]
class GenerateDatasetCsvCommand extends Command
{
    public const GENDER = ['female', 'male'];
    public const COUNTRIES = ['England', 'France', 'Germany'];
    public const LANGUAGES = [
        'England' => ['en'],
        'France' => ['fr'],
        'Germany' => ['de'],
    ];
    public const TOPICS = [
        "Adidas",
        "Adobe",
        "Airbnb",
        "Aldi",
        "Amazon",
        "AMD",
        "Apple",
        "Audi",
        "Bayer",
        "BMW",
        "Budweiser",
        "Burberry",
        "Burger King",
        "Cadillac",
        "Cartier",
        "Coca Cola",
        "Coop",
        "Credit Suisse",
        "Decathlon",
        "Delta",
        "DHL",
        "Dior",
        "Disney",
        "Dove",
        "FedEx",
        "Ferrari",
        "Ford",
        "Google",
        "Gucci",
        "H&M",
        "Heineken",
        "HP",
        "Huawei",
        "IBM",
        "Ikea",
        "Instagram",
        "Intel",
        "iPad",
        "iPhone",
        "Jack & Jones",
        "Kelloggs",
        "KFC",
        "L'Oréal",
        "Landi",
        "Lego",
        "Lenovo",
        "Lidl",
        "LinkedIn",
        "Louis Vuitton",
        "Lufthansa",
        "MasterCard",
        "Mazda",
        "McDonalds",
        "Mercedes Benz",
        "Microsoft",
        "Migros",
        "Netflix",
        "Nike",
        "Nissan",
        "Nivea",
        "Nokia",
        "Nvidia",
        "Oracle",
        "Pepsi",
        "Pizza Hut",
        "Playmobil",
        "PlayStation",
        "Porsche",
        "Prada",
        "Puma",
        "Red Bull",
        "Roche",
        "Rolex",
        "Salesforce",
        "Salt",
        "Samsung",
        "Sky",
        "Sony",
        "Spotify",
        "Starbucks",
        "Subaru",
        "Subway",
        "Swisscom",
        "Tesla",
        "TikTok",
        "Tinder",
        "Toyota",
        "Uber",
        "UBS",
        "Uniqlo",
        "UPS",
        "Visa",
        "Volvo",
        "VW",
        "WhatsApp",
        "Xbox",
        "Xing",
        "YouTube",
        "Zara",
        "Zweifel",
    ];

    // Configuration for small test data set
    protected int $numberOfPersonsPerCountry = 20000;
    protected int $numberOfRegionalFriendshipsMin = 10;
    protected int $numberOfRegionalFriendshipsMax = 40;
    protected int $numberOfNationalFriendshipsMin = 5;
    protected int $numberOfNationalFriendshipsMax = 30;
    protected int $numberOfInternationalFriendshipsMin = 1;
    protected int $numberOfInternationalFriendshipsMax = 10;
    protected float $probabilityForRegionalFriendships = 0.6;
    protected float $probabilityForNationalFriendships = 0.6;
    protected float $probabilityForInternationalFriendships = 0.05;
    protected float $probabilityForInternationalLanguage = 0.05;

    protected int $numberOfLikesMin = 2;
    protected int $numberOfLikesMax = 10;
    protected float $probabilityForLikes = 0.6;

    protected $persons = [];
    protected $personsByCountry = [
        'England' => [],
        'France' => [],
        'Germany' => [],
    ];
    protected $personsOutsideOfCountry = [
        'England' => [],
        'France' => [],
        'Germany' => [],
    ];
    protected $topics = [];

    protected \DateTime $creationDate;

    public function __construct(private ManagerRegistry $managerRegistry)
    {
        parent::__construct();

        $this->creationDate = new \DateTime("2020-01-01");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->createDataset();
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('Dataset generated and stored as csv files in `/dataset`.');

        return Command::SUCCESS;
    }

    private function createDataset(): void
    {
        $this->createPersons();
        $this->createFriendships();
        $this->createTopics();
        $this->createLikes();
    }

    private function createPersons(): void
    {
        $faker = \Faker\Factory::create();

        $fp = fopen('../dataset/persons.csv', 'w');
        $headers = ['id', 'gender', 'first_name', 'last_name', 'language', 'country', 'birth_date'];
        fputcsv($fp, $headers);

        $pId = 1;
        foreach (self::COUNTRIES as $country) {
            for ($p = 0; $p < $this->numberOfPersonsPerCountry; $p++) {
                $gender = $this->getRandomGender();
                $language = (rand(0, 100) / 100) < $this->probabilityForInternationalLanguage
                    ? $this->getRandomInternationalLanguage($country)
                    : $this->getRandomRegionalLanguage($country);

                if ($pId === 112) {
                    $firstName = 'John';
                    $lastName = 'Doe';
                } else {
                    $firstName = $faker->firstName($gender);
                    $lastName = $faker->lastName();
                }

                $person = [
                    'id' => $pId,
                    'gender' => $gender,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'language' => $language,
                    'country' => $country,
                    'birth_date' => $faker->dateTimeBetween('-90 years', '-10 years')->format('Y-m-d'),
                ];

                fputcsv($fp, $person);

                $this->persons[] = $person;
                $this->personsByCountry[$country][] = $person;
                foreach (self::COUNTRIES as $invertedCountry) {
                    if ($country !== $invertedCountry) {
                        $this->personsOutsideOfCountry[$invertedCountry][] = $person;
                    }
                }
                $pId++;
            }
        }

        fclose($fp);
    }

    private function createFriendships(): void
    {
        $headers = ['origin_id', 'target_id', 'created_at'];

        $fp1 = fopen('../dataset/friendships_1.csv', 'w');
        fputcsv($fp1, $headers);

        $fp2 = fopen('../dataset/friendships_2.csv', 'w');
        fputcsv($fp2, $headers);

        $fp3 = fopen('../dataset/friendships_3.csv', 'w');
        fputcsv($fp3, $headers);

        $fp4 = fopen('../dataset/friendships_4.csv', 'w');
        fputcsv($fp4, $headers);

        foreach (self::COUNTRIES as $country) {
            // Algorithm 1
            $startWith = 0;
            while ($startWith < $this->numberOfPersonsPerCountry) {
                $items = array_slice($this->personsByCountry[$country], $startWith, 10);
                if (count($items) !== 10) {
                    break;
                }

                for ($s = 0; $s < 5; $s++) {
                    $sourceNode = $items[$s];
                    for ($t = $s + 1; $t < 10 - $s; $t++) {
                        $targetNode = $items[$t];

                        fputcsv($fp1, [
                            'origin_id' => $sourceNode['id'],
                            'target_id' => $targetNode['id'],
                            'created_at' => $this->createRandomCreatedAtDate(),
                        ]);
                    }
                }

                $startWith += 8;
            }

            // Algorithm 2
            $startWith = 0;
            while ($startWith < $this->numberOfPersonsPerCountry) {
                $items = array_slice($this->personsByCountry[$country], $startWith, 100);
                if (count($items) !== 100) {
                    break;
                }

                $items1 = array_slice($items, 0, 40);
                $items2 = array_slice($items, 50, 40);

                foreach ($items1 as $item) {
                    if (rand(0, 100) / 100 < $this->probabilityForRegionalFriendships) {
                        // Pick random targets
                        $numberOfEdges = rand($this->numberOfRegionalFriendshipsMin, $this->numberOfRegionalFriendshipsMax);
                        $targetKeys = array_rand($items2, $numberOfEdges);

                        foreach ($targetKeys as $targetKey) {
                            fputcsv($fp2, [
                                'origin_id' => $item['id'],
                                'target_id' => $items2[$targetKey]['id'],
                                'created_at' => $this->createRandomCreatedAtDate(),
                            ]);
                        }
                    }
                }

                $startWith += 100;
            }

            for ($c = 0; $c < count($this->personsByCountry[$country]); $c++) {
                // Algorithm 3: Random friendships upwards in country
                if (rand(0, 100) / 100 < $this->probabilityForNationalFriendships) {
                    $numberOfEdges = rand($this->numberOfNationalFriendshipsMin, $this->numberOfNationalFriendshipsMax);
                    $targetKeys = [];
                    for ($i = 0; $i < $numberOfEdges; $i++) {
                        $rangeFrom = $c + 100;
                        $rangeTo = $this->numberOfPersonsPerCountry - 1;

                        if ($rangeFrom >= $rangeTo) {
                            continue;
                        }

                        $targetKeys[] = rand($rangeFrom, $rangeTo);
                    }

                    $targetKeys = array_unique($targetKeys);
                    foreach ($targetKeys as $targetKey) {
                        fputcsv($fp3, [
                            'origin_id' => $this->personsByCountry[$country][$c]['id'],
                            'target_id' => $this->personsByCountry[$country][$targetKey]['id'],
                            'created_at' => $this->createRandomCreatedAtDate(),
                        ]);
                    }
                }

                // Algorithm 4: International friendships
                if ((rand(0, 100) / 100) < $this->probabilityForInternationalFriendships) {
                    $numberOfConnectionsToGenerate = rand(
                        $this->numberOfInternationalFriendshipsMin,
                        $this->numberOfInternationalFriendshipsMax
                    );

                    $targetKeys = [];
                    for ($ic = 0; $ic < $numberOfConnectionsToGenerate; $ic++) {
                        while (count($targetKeys) < $numberOfConnectionsToGenerate) {
                            $key = array_rand($this->personsOutsideOfCountry[$country]);
                            if (!in_array($key, $targetKeys)) {
                                $targetKeys[] = $key;
                            }
                        }
                    }

                    foreach ($targetKeys as $targetKey) {
                        fputcsv($fp4, [
                            'origin_id' => $this->personsByCountry[$country][$c]['id'],
                            'target_id' => $this->personsOutsideOfCountry[$country][$targetKey]['id'],
                            'created_at' => $this->createRandomCreatedAtDate(),
                        ]);
                    }
                }
            }
        }

        fclose($fp1);
        fclose($fp2);
        fclose($fp3);
        fclose($fp4);
    }

    private function createTopics(): void
    {
        $fp = fopen('../dataset/topics.csv', 'w');
        $headers = ['id', 'name'];
        fputcsv($fp, $headers);

        $tId = 1;
        foreach (self::TOPICS as $topicName) {
            $topic = [
                'id' => $tId,
                'name' => $topicName,
            ];
            fputcsv($fp, $topic);
            $this->topics[] = $topic;
            $tId++;
        }

        fclose($fp);
    }

    private function createLikes(): void
    {
        $fp = fopen('../dataset/likes.csv', 'w');
        $headers = ['person_id', 'topic_id', 'created_at'];
        fputcsv($fp, $headers);

        for ($i = 0; $i < count(self::COUNTRIES); $i++) {
            $offset = $i * $this->numberOfPersonsPerCountry + 1;

            for ($p = 0; $p < $this->numberOfPersonsPerCountry; $p++) {
                $personId = $offset + $p;

                if (rand(0, 100) / 100 < $this->probabilityForLikes) {
                    // Generate likes
                    $numberOfLikesToGenerate = rand(
                        $this->numberOfLikesMin,
                        $this->numberOfLikesMax
                    );

                    $randomTopics = array_rand($this->topics, $numberOfLikesToGenerate);
                    foreach ($randomTopics as $randomTopicIndex) {
                        fputcsv($fp, [
                            'person_id' => $personId,
                            'topic_id' => $this->topics[$randomTopicIndex]['id'],
                            'created_at' => $this->createRandomCreatedAtDate(),
                        ]);
                    }
                }
            }
        }

        fclose($fp);
    }

    private function getRandomGender(): string
    {
        $gender = self::GENDER;

        return $gender[array_rand($gender)];
    }

    private function getRandomInternationalLanguage(string $region): string
    {
        $internationalLanguages = [];

        foreach (self::LANGUAGES as $reg => $languages) {
            if ($region !== $reg) {
                $internationalLanguages = array_merge($internationalLanguages, $languages);
            }
        }

        return $internationalLanguages[array_rand($internationalLanguages)];
    }

    private function getRandomRegionalLanguage(string $region): string
    {
        $languages = self::LANGUAGES[$region];

        return $languages[array_rand($languages)];
    }

    private function createRandomCreatedAtDate(): string
    {
        $randomMinutes = mt_rand(10, 120);
        $interval = new \DateInterval('PT' . $randomMinutes . 'S');
        $this->creationDate->add($interval);

        return $this->creationDate->format('Y-m-d H:i:s');
    }
}
