<?php

namespace App\Command;

use App\Utility\MysqlQueryCollection;
use App\Utility\Neo4jQueryCollection;
use Doctrine\DBAL\Connection;
use Laudis\Neo4j\ClientBuilder;
use Laudis\Neo4j\Contracts\ClientInterface;
use Laudis\Neo4j\Databags\DriverConfiguration;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

#[AsCommand(
    name: 'app:run-evaluation',
    description: 'Run evaluation and store results',
)]
class EvaluationCommand extends Command
{
    public const DB_MYSQL = 'mysql';
    public const DB_NEO4J = 'neo4j';

    public const USECASE_AUTO_SUGGEST = 'auto-suggest';
    public const USECASE_FRIENDS_LIST = 'friends-list';
    public const USECASE_FRIENDS_RECOMMENDATION = 'friends-recommendation';
    public const USECASE_FRIENDS_RECOMMENDATION_2 = 'friends-recommendation-2';

    // Configuration for small test data set
    protected int $numberOfTests = 50;
    protected array $userIds = [112, 2624, 19143, 55745];
    protected string $searchQuery = 'vi';
    protected array $results = [];

    protected ClientInterface $neo4jClient;

    public function __construct(
        private readonly Connection $connection,
        private readonly Stopwatch $stopwatch,
    )
    {
        parent::__construct();

        $this->neo4jClient = ClientBuilder::create()
            ->withDriver('neo4j', 'bolt://neo4j:MasterThesis23!@192.168.0.203')
            ->withDefaultDriver('neo4j')
            ->build();

        $this->neo4jClient->verifyConnectivity();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->runEvaluation();
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('Ran evaluation and stored results in `/evaluation`.');

        return Command::SUCCESS;
    }

    protected function runEvaluation(): void
    {
        $this->runWarmupQueries($this->userIds[0]);

        for ($i = 0; $i < $this->numberOfTests; $i++) {
            foreach ($this->userIds as $userId) {
                $this->runFriendsListEvaluation($userId);
                $this->runFriendsRecommendationEvaluation($userId);
                $this->runFriendsRecommendation2Evaluation($userId);
                $this->runAutoSuggestEvaluation($userId);
                $this->stopwatch->reset();
            }
        }

        $this->exportResults();
    }

    private function runWarmupQueries(int $userId)
    {
        $mysqlQuery = MysqlQueryCollection::getFriendsListQuery($userId);
        $neo4jQuery = Neo4jQueryCollection::getFriendsListQuery($userId);

        $this->connection->fetchAllAssociative($mysqlQuery);
        $this->neo4jClient->run($neo4jQuery);
    }

    private function runFriendsListEvaluation(int $userId)
    {
        $mysqlQuery = MysqlQueryCollection::getFriendsListQuery($userId);
        $neo4jQuery = Neo4jQueryCollection::getFriendsListQuery($userId);

        $this->stopwatch->start(self::DB_MYSQL . '_' . self::USECASE_FRIENDS_LIST);
        $results = $this->connection->fetchAllAssociative($mysqlQuery);
        foreach ($results as $result) {}
        $mysqlEvent = $this->stopwatch->stop(self::DB_MYSQL . '_' . self::USECASE_FRIENDS_LIST);

        $this->recordMysqlResult(self::USECASE_FRIENDS_LIST, $userId, $mysqlEvent->getDuration());

        $this->stopwatch->start(self::DB_NEO4J . '_' . self::USECASE_FRIENDS_LIST);
        $results = $this->neo4jClient->run($neo4jQuery);
        foreach ($results->getResults() as $result) {}
        $neo4jEvent = $this->stopwatch->stop(self::DB_NEO4J . '_' . self::USECASE_FRIENDS_LIST);

        $this->recordNeo4jResult(self::USECASE_FRIENDS_LIST, $userId, $neo4jEvent->getDuration());
    }

    private function runFriendsRecommendationEvaluation(int $userId)
    {
        $mysqlQuery = MysqlQueryCollection::getFriendsRecommendationQuery($userId);
        $neo4jQuery = Neo4jQueryCollection::getFriendsRecommendationQuery($userId);

        $this->stopwatch->start(self::DB_MYSQL . '_' . self::USECASE_FRIENDS_RECOMMENDATION);
        $results = $this->connection->fetchAllAssociative($mysqlQuery);
        foreach ($results as $result) {}
        $mysqlEvent = $this->stopwatch->stop(self::DB_MYSQL . '_' . self::USECASE_FRIENDS_RECOMMENDATION);

        $this->recordMysqlResult(self::USECASE_FRIENDS_RECOMMENDATION, $userId, $mysqlEvent->getDuration());

        $this->stopwatch->start(self::DB_NEO4J . '_' . self::USECASE_FRIENDS_RECOMMENDATION);
        $results = $this->neo4jClient->run($neo4jQuery);
        foreach ($results->getResults() as $result) {}
        $neo4jEvent = $this->stopwatch->stop(self::DB_NEO4J . '_' . self::USECASE_FRIENDS_RECOMMENDATION);

        $this->recordNeo4jResult(self::USECASE_FRIENDS_RECOMMENDATION, $userId, $neo4jEvent->getDuration());
    }

    private function runFriendsRecommendation2Evaluation(int $userId)
    {
        $mysqlQuery = MysqlQueryCollection::getFriendsRecommendation2Query($userId);
        $neo4jQuery = Neo4jQueryCollection::getFriendsRecommendation2Query($userId);

        $this->stopwatch->start(self::DB_MYSQL . '_' . self::USECASE_FRIENDS_RECOMMENDATION_2);
        $results = $this->connection->fetchAllAssociative($mysqlQuery);
        foreach ($results as $result) {}
        $mysqlEvent = $this->stopwatch->stop(self::DB_MYSQL . '_' . self::USECASE_FRIENDS_RECOMMENDATION_2);

        $this->recordMysqlResult(self::USECASE_FRIENDS_RECOMMENDATION_2, $userId, $mysqlEvent->getDuration());

        $this->stopwatch->start(self::DB_NEO4J . '_' . self::USECASE_FRIENDS_RECOMMENDATION_2);
        $results = $this->neo4jClient->run($neo4jQuery);
        foreach ($results->getResults() as $result) {}
        $neo4jEvent = $this->stopwatch->stop(self::DB_NEO4J . '_' . self::USECASE_FRIENDS_RECOMMENDATION_2);

        $this->recordNeo4jResult(self::USECASE_FRIENDS_RECOMMENDATION_2, $userId, $neo4jEvent->getDuration());
    }

    private function runAutoSuggestEvaluation(int $userId)
    {
        $mysqlQuery = MysqlQueryCollection::getAutoSuggestQuery($userId, $this->searchQuery);
        $neo4jQuery = Neo4jQueryCollection::getAutoSuggestQuery($userId, $this->searchQuery);

        $this->stopwatch->start(self::DB_MYSQL . '_' . self::USECASE_AUTO_SUGGEST);
        $results = $this->connection->fetchAllAssociative($mysqlQuery);
        foreach ($results as $result) {}
        $mysqlEvent = $this->stopwatch->stop(self::DB_MYSQL . '_' . self::USECASE_AUTO_SUGGEST);

        $this->recordMysqlResult(self::USECASE_AUTO_SUGGEST, $userId, $mysqlEvent->getDuration());

        $this->stopwatch->start(self::DB_NEO4J . '_' . self::USECASE_AUTO_SUGGEST);
        $results = $this->neo4jClient->run($neo4jQuery);
        foreach ($results->getResults() as $result) {}
        $neo4jEvent = $this->stopwatch->stop(self::DB_NEO4J . '_' . self::USECASE_AUTO_SUGGEST);

        $this->recordNeo4jResult(self::USECASE_AUTO_SUGGEST, $userId, $neo4jEvent->getDuration());
    }

    private function recordMysqlResult(string $useCase, int $userId, float $duration): void
    {
        $this->recordResult(self::DB_MYSQL, $useCase, $userId, $duration);
    }

    private function recordNeo4jResult(string $useCase, int $userId, float $duration): void
    {
        $this->recordResult(self::DB_NEO4J, $useCase, $userId, $duration);
    }

    private function recordResult(string $dbEngine, string $useCase, int $userId, float $duration): void
    {
        $this->results[$useCase][$userId][$dbEngine][] = $duration;
    }

    private function exportResults()
    {
        foreach ($this->results as $useCase => $useCaseData) {
            $this->createCsvFile($useCase, $useCaseData);
        }
    }

    private function createCsvFile(string $useCase, array $data): void
    {
        $fileName = $useCase . '.csv';
        $fp = fopen('../evaluation/' . $fileName, 'w');
        fputcsv($fp, ['User', 'MySQL', 'Neo4j']);
        foreach ($data as $userId => $userData) {
            for ($i = 0; $i < count($userData[self::DB_MYSQL]); $i++) {
                fputcsv($fp, ['User ' . $userId, $userData[self::DB_MYSQL][$i], $userData[self::DB_NEO4J][$i]]);
            }
        }

        fclose($fp);
    }
}
