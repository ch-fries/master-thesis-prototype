<?php

namespace App\Entity;

use App\Repository\FriendshipRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FriendshipRepository::class)]
class Friendship
{
    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'owningConnections')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Person $origin = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'incomingConnections')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Person $target = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    public function getOrigin(): ?Person
    {
        return $this->origin;
    }

    public function setOrigin(?Person $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getTarget(): ?Person
    {
        return $this->target;
    }

    public function setTarget(?Person $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = \DateTimeImmutable::createFromMutable($createdAt);

        return $this;
    }
}
