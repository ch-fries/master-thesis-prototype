<?php

namespace App\Controller\Playground;

use Laudis\Neo4j\ClientBuilder;
use Laudis\Neo4j\Contracts\ClientInterface;
use Laudis\Neo4j\Databags\SummarizedResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Neo4jController extends AbstractController
{
    #[Route('/playground/neo4j/friends', name: 'neo4j_friends')]
    public function index(): Response
    {
        $client = $this->getClient();
        /** @var SummarizedResult $result */
        $result = $client->run('MATCH (me:Person {personId: 112})-[:IS_FRIENDS_WITH]-(f:Person) RETURN f');

        return $this->render('neo4j/friends.html.twig', [
            'persons' => [],
            'result' => $result,
        ]);
    }

    private function getClient(): ClientInterface
    {
        return ClientBuilder::create()
            ->withDriver('neo4j', 'neo4j://neo4j:MasterThesis23!@192.168.0.203')
            ->withDefaultDriver('neo4j')
            ->build();
    }
}