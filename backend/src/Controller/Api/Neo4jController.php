<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Utility\Neo4jQueryCollection;
use Laudis\Neo4j\ClientBuilder;
use Laudis\Neo4j\Contracts\ClientInterface;
use Laudis\Neo4j\Databags\SessionConfiguration;
use Laudis\Neo4j\Types\CypherMap;
use Laudis\Neo4j\Types\Node;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Stopwatch\Stopwatch;

class Neo4jController extends AbstractController
{
    public function __construct(
        private readonly Stopwatch $stopwatch
    ) {
    }

    #[Route('/api/neo4j/friends', name: 'api_neo4j_friends')]
    public function friends(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        if ($userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();
        $query = Neo4jQueryCollection::getFriendsListQuery($userId);

        $this->stopwatch->start('neo4j_friends');
        $results = $client->run($query);

        $friends = [];
        /** @var CypherMap $result */
        foreach ($results->getResults() as $result) {
            $record = [
                'id' => $result->get('p.personId'),
                'firstName' => $result->get('p.firstName'),
                'lastName' => $result->get('p.lastName'),
            ];
            $friends[] = $record;
        };

        $event = $this->stopwatch->stop('neo4j_friends');

        $response = [
            'data' => $friends,
            'count' => count($friends),
            'duration' => $event->getDuration(),
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/neo4j/friends-recommendation', name: 'api_neo4j_friends-recommendation')]
    public function friendsRecommendation(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        if ($userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }
        $client = $this->getClient();
        $query = Neo4jQueryCollection::getFriendsRecommendationQuery($userId);

        $this->stopwatch->start('neo4j_friends_recommendation');
        $results = $client->run($query);

        $recommendations = [];
        /** @var CypherMap $result */
        foreach ($results->getResults() as $result) {
            /** @var Node $f */
            $f = $result->get('friend');
            $recommendation = [
                'id' => $f->getProperty('personId'),
                'firstName' => $f->getProperty('firstName'),
                'lastName' => $f->getProperty('lastName'),
                'mutualFriends' => $result->get('mutualFriends'),
            ];
            $recommendations[] = $recommendation;
        };
        $event = $this->stopwatch->stop('neo4j_friends_recommendation');

        $response = [
            'data' => $recommendations,
            'count' => count($recommendations),
            'duration' => $event->getDuration(),
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/neo4j/friends-recommendation-2', name: 'api_neo4j_friends-recommendation-2')]
    public function friendsRecommendation2(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        if ($userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }
        $client = $this->getClient();
        $query = Neo4jQueryCollection::getFriendsRecommendation2Query($userId);

        $this->stopwatch->start('neo4j_friends_recommendation-2');
        $results = $client->run($query);

        $recommendations = [];
        /** @var CypherMap $result */
        foreach ($results->getResults() as $result) {
            /** @var Node $f */
            $f = $result->get('friend');
            $recommendation = [
                'id' => $f->getProperty('personId'),
                'firstName' => $f->getProperty('firstName'),
                'lastName' => $f->getProperty('lastName'),
                'weight' => $result->get('score'),
            ];
            $recommendations[] = $recommendation;
        };
        $event = $this->stopwatch->stop('neo4j_friends_recommendation-2');

        $response = [
            'data' => $recommendations,
            'count' => count($recommendations),
            'duration' => $event->getDuration(),
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/neo4j/auto-suggest', name: 'api_neo4j_auto-suggest')]
    public function autoSuggest(Request $request): Response
    {
        $searchQuery = strtolower($request->get('s', ''));
        $userId = (int)$request->get('userId');
        if ($searchQuery === '' || $userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();
        $query = Neo4jQueryCollection::getAutoSuggestQuery($userId, $searchQuery);

        $this->stopwatch->start('neo4j_auto_suggest');

        $results = $client->run($query);

        $persons = [];
        /** @var CypherMap $result */
        foreach ($results->getResults() as $result) {
            $persons[] = [
                'id' => $result->get('id'),
                'label' => $result->get('label'),
                'type' => $result->get('type'),
                'score' => $result->get('score'),
                'connected' => $result->get('connected'),
            ];
        };
        $event = $this->stopwatch->stop('neo4j_auto_suggest');

        $response = [
            'data' => $persons,
            'count' => count($persons),
            'duration' => $event->getDuration(),
        ];
        return new JsonResponse($response);
    }

    #[Route('/api/neo4j/profile', name: 'api_neo4j_profile')]
    public function profile(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('profileId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();

        $queryForTopics = Neo4jQueryCollection::getTopics($userId, $profileId);
        $topics = $client->run($queryForTopics);

        $ts = [];
        foreach ($topics->getResults() as $result) {
            $t = $result->get('t');
            $ts[] = [
                'id' => $t->getProperty('topicId'),
                'name' => $t->getProperty('name'),
                'mutual' => $result->get('mutual'),
            ];
        }

        $queryForFriends = Neo4jQueryCollection::getFriends($userId, $profileId);
        $friends = $client->run($queryForFriends);

        $fs = [];
        foreach ($friends->getResults() as $result) {
            $f = $result->get('p');
            $fs[] = [
                'id' => $f->getProperty('personId'),
                'first_name' => $f->getProperty('firstName'),
                'last_name' => $f->getProperty('lastName'),
                'mutual' => $result->get('mutual'),
            ];
        }

        $queryForPerson = Neo4jQueryCollection::getPerson($userId, $profileId);
        $person = $client->run($queryForPerson);
        $p = $person[0]->get('p');
        $person = [
            'id' => $p->getProperty('personId'),
            'firstName' => $p->getProperty('firstName'),
            'lastName' => $p->getProperty('lastName'),
            'gender' => $p->getProperty('gender'),
            'isFriend' => $person[0]->get('isFriend'),
        ];

        $response = [
            'person' => $person,
            'topics' => $ts,
            'friends' => $fs,
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/neo4j/topic', name: 'api_neo4j_topic')]
    public function topic(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $topicId = (int)$request->get('topicId');
        if ($userId === 0 || $topicId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();
        $queryForTopics = Neo4jQueryCollection::getPeopleLikingTopic($userId, $topicId);
        $topics = $client->run($queryForTopics);

        $persons = [];
        foreach ($topics->getResults() as $result) {
            $f = $result->get('p');
            $persons[] = [
                'id' => $f->getProperty('personId'),
                'first_name' => $f->getProperty('firstName'),
                'last_name' => $f->getProperty('lastName'),
                'mutual' => $result->get('mutual'),
            ];
        }

        $queryForTopic = Neo4jQueryCollection::getTopic($userId, $topicId);
        $topics = $client->run($queryForTopic);
        $p = $topics[0]->get('t');
        $topic = [
            'id' => $p->getProperty('topicId'),
            'name' => $p->getProperty('name'),
            'likes' => $topics[0]->get('likes'),
        ];

        $response = [
            'topic' => $topic,
            'persons' => $persons,
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/neo4j/friend', name: 'api_neo4j_add-friend', methods: 'POST')]
    public function addFriend(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('profileId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();
        $query = Neo4jQueryCollection::addFriend($userId, $profileId);
        $client->run($query);

        return new JsonResponse();
    }

    #[Route('/api/neo4j/friend', name: 'api_neo4j_remove-friend', methods: 'DELETE')]
    public function removeFriend(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('profileId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();
        $query = Neo4jQueryCollection::removeFriend($userId, $profileId);
        $client->run($query);

        return new JsonResponse();
    }

    #[Route('/api/neo4j/like', name: 'api_neo4j_like', methods: 'POST')]
    public function like(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('topicId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();
        $query = Neo4jQueryCollection::like($userId, $profileId);
        $client->run($query);

        return new JsonResponse();
    }

    #[Route('/api/neo4j/like', name: 'api_neo4j_unlike', methods: 'DELETE')]
    public function unlike(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('topicId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $client = $this->getClient();
        $query = Neo4jQueryCollection::unlike($userId, $profileId);
        $client->run($query);

        return new JsonResponse();
    }

    private function lowLevel(string $query)
    {
        $conn = new \Bolt\connection\Socket('192.168.0.203', 7687);
        $bolt = new \Bolt\Bolt($conn);
        $bolt->setProtocolVersions(5, 4.4);
        $protocol = $bolt->build();
        $protocol->hello(\Bolt\helpers\Auth::basic('neo4j', 'MasterThesis23!'));

        $protocol
            ->run($query)
            ->pull();

        $responses = $protocol->getResponses();
        $rsps = [];
        foreach ($responses as $response) {
            $rsps[] = $response;
        }

        return $rsps;
    }

    private function getClient(): ClientInterface
    {
        return ClientBuilder::create()
            ->withDriver('bolt', 'bolt://neo4j:MasterThesis23!@192.168.0.203')
//            ->withDriver('neo4j', 'bolt://neo4j:MasterThesis23!@host.docker.internal:7687')
            ->withDefaultDriver('bolt')
            ->withDefaultSessionConfiguration(SessionConfiguration::default()->withDatabase('neo4j'))
            ->build();
    }
}
