<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Utility\MysqlQueryCollection;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Stopwatch\Stopwatch;

class MysqlController extends AbstractController
{
    public function __construct(
        private readonly Connection $connection,
        private readonly Stopwatch $stopwatch
    ) {}

    #[Route('/api/mysql/friends', name: 'api_mysql_friends')]
    public function friends(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        if ($userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $query = MysqlQueryCollection::getFriendsListQuery($userId);

        $this->stopwatch->start('mysql_friends');
        $results = $this->connection->fetchAllAssociative($query);

        $friends = [];
        foreach ($results as $result) {
            $friend = [
                'id' => $result['id'],
                'firstName' => $result['first_name'],
                'lastName' => $result['last_name'],
            ];
            $friends[] = $friend;
        }
        $event = $this->stopwatch->stop('mysql_friends');

        $response = [
            'data' => $friends,
            'count' => count($friends),
            'duration' => $event->getDuration(),
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/mysql/friends-recommendation', name: 'api_mysql_friends-recommendation')]
    public function friendsRecommendation(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        if ($userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $query = MysqlQueryCollection::getFriendsRecommendationQuery($userId);

        $this->stopwatch->start('mysql_friends_recommendation');
        $results = $this->connection->fetchAllAssociative($query);

        $recommendations = [];
        foreach ($results as $result) {
            $recommendation = [
                'id' => $result['id'],
                'firstName' => $result['first_name'],
                'lastName' => $result['last_name'],
                'mutualFriends' => $result['mutual_friends'],
            ];
            $recommendations[] = $recommendation;
        }
        $event = $this->stopwatch->stop('mysql_friends_recommendation');

        $response = [
            'data' => $recommendations,
            'count' => count($recommendations),
            'duration' => $event->getDuration(),
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/mysql/friends-recommendation-2', name: 'api_mysql_friends-recommendation-2')]
    public function friendsRecommendation2(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        if ($userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $query = MysqlQueryCollection::getFriendsRecommendation2Query($userId);

        $this->stopwatch->start('mysql_friends_recommendation-2');
        $results = $this->connection->fetchAllAssociative($query);

        $recommendations = [];
        foreach ($results as $result) {
            $recommendation = [
                'id' => $result['id'],
                'firstName' => $result['first_name'],
                'lastName' => $result['last_name'],
                'weight' => $result['finalScore'],
            ];
            $recommendations[] = $recommendation;
        }
        $event = $this->stopwatch->stop('mysql_friends_recommendation-2');

        $response = [
            'data' => $recommendations,
            'count' => count($recommendations),
            'duration' => $event->getDuration(),
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/mysql/auto-suggest', name: 'api_mysql_auto-suggest')]
    public function autoSuggest(Request $request): Response
    {
        $searchQuery = strtolower($request->get('s', ''));
        $userId = (int)$request->get('userId');
        if ($searchQuery === '' || $userId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $query = MysqlQueryCollection::getAutoSuggestQuery($userId, $searchQuery);

        $this->stopwatch->start('mysql_auto_suggest');
        $results = $this->connection->fetchAllAssociative($query);

        $suggestions = [];
        foreach ($results as $result) {
            $suggestion = [
                'id' => $result['id'],
                'label' => $result['label'],
                'type' => $result['type'],
                'score' => $result['score'],
                'connected' => $result['connected'],
            ];
            $suggestions[] = $suggestion;
        }
        $event = $this->stopwatch->stop('mysql_auto_suggest');

        $response = [
            'data' => $suggestions,
            'count' => count($suggestions),
            'duration' => $event->getDuration(),
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/mysql/profile', name: 'api_mysql_profile')]
    public function profile(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('profileId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $queryForPerson = MysqlQueryCollection::getPerson($userId, $profileId);
        $person = $this->connection->fetchAssociative($queryForPerson);

        $queryForTopics = MysqlQueryCollection::getTopics($userId, $profileId);
        $topics = $this->connection->fetchAllAssociative($queryForTopics);

        $queryForFriends = MysqlQueryCollection::getFriends($userId, $profileId);
        $friends = $this->connection->fetchAllAssociative($queryForFriends);

        $response = [
            'person' => $person,
            'topics' => $topics,
            'friends' => $friends,
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/mysql/topic', name: 'api_mysql_topic')]
    public function topic(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $topicId = (int)$request->get('topicId');
        if ($userId === 0 || $topicId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $queryForTopic = MysqlQueryCollection::getTopic($userId, $topicId);
        $topic = $this->connection->fetchAssociative($queryForTopic);

        $queryForPersons = MysqlQueryCollection::getPeopleLikingTopic($userId, $topicId);
        $persons = $this->connection->fetchAllAssociative($queryForPersons);

        $response = [
            'topic' => $topic,
            'persons' => $persons,
        ];

        return new JsonResponse($response);
    }

    #[Route('/api/mysql/friend', name: 'api_mysql_add-friend', methods: 'POST')]
    public function addFriend(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('profileId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $queryForPersons = MysqlQueryCollection::addFriend($userId, $profileId);
        $this->connection->executeQuery($queryForPersons);

        return new JsonResponse();
    }

    #[Route('/api/mysql/friend', name: 'api_mysql_remove-friend', methods: 'DELETE')]
    public function removeFriend(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('profileId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $queryForPersons = MysqlQueryCollection::removeFriend($userId, $profileId);
        $this->connection->executeQuery($queryForPersons);

        return new JsonResponse();
    }

    #[Route('/api/mysql/like', name: 'api_mysql_like', methods: 'POST')]
    public function like(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('topicId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $queryForPersons = MysqlQueryCollection::like($userId, $profileId);
        $this->connection->executeQuery($queryForPersons);

        return new JsonResponse();
    }

    #[Route('/api/mysql/unlike', name: 'api_mysql_unlike', methods: 'DELETE')]
    public function unlike(Request $request): Response
    {
        $userId = (int)$request->get('userId');
        $profileId = (int)$request->get('topicId');
        if ($userId === 0 || $profileId === 0) {
            return new JsonResponse('Invalid request', 400);
        }

        $queryForPersons = MysqlQueryCollection::unlike($userId, $profileId);
        $this->connection->executeQuery($queryForPersons);

        return new JsonResponse();
    }
}