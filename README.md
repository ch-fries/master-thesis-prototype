# Master Thesis

The code in this repository was developed during my master thesis _Graph Databases vs. Relational Databases for Social Web Application_.

## Architecture

The application is designed as client server application with two different database engines:

![Schema of architecture of application](./docs/img/architecture.png "Architecture of the application")

* Frontend application based an [Angular](https://angular.io/)
* REST API based on [Symfony](https://symfony.com/)
* MySQL database server
* Neo4j database server

## Structure

* The REST API project is in the `backend` folder
* The frontend application source code is in the `frontend` folder
* The database servers have to be set up manually. Read chapter 3 of the thesis for more information

## Getting started

To run the project locally, use [DDEV](https://ddev.readthedocs.io), which stands for docker driven development.

```shell
cd backend
ddev start
ddev composer install
```

Access the application in a browser with https://master-thesis.ddev.site/.

## Development

### Enable dev context for REST API

To put the symfony project into development mode, create a file `.env.local` and add this line:
```
APP_ENV=dev
```

### Development server for frontend application with live reload

To run a development server for the frontend application, run:
```shell
cd frontend
npm start
```

### Build a new version 

To build a new version of the frontend application, run:
```shell
cd frontend
npm run-script build
```

The build artifacts will be put in `frontend/dist/frontend`. If you want the new build to be served from https://master-thesis.ddev.site/, replace the files in `backend/public` except for:
* build
* .htaccess
* index.php